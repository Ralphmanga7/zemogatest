//
//  Loading.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import UIKit

class Loading: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    class func instanceFromNib() -> UIView {
        let view = UINib(nibName: "Loading", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! Loading
        return view
    }

    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.cornerRadius = 6
        self.layer.masksToBounds = true
    }

}
