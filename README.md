# Zemoga Mobile Test
  
 
- CocoaPods
- iOS 13+
- Xcode 11.2+
- Swift 5

## Instructions

- Open Terminal
- Navigate to the project folder and run:

```bash
pod install
```
## Usage
- Open ZemogaTest.xcworkspace

## Project Features

Create an app that lists all messages and their details from [JSONPlaceholder](https://jsonplaceholder.typicode.com/)

### Comments
Based on the MVVM architecture, this allows a single responsibility to its corresponding file, this reduce the size of massive view controllers by moving logic into the view model and MVVM makes it easy to test the logic behind the views.

### Third Party Pods

I used Third party libraries to facilitate the execution of the tasks. The libraries are well maintained and are among the most popular Swift frameworks available.

- [RealmSwift](https://github.com/realm/realm-cocoa): It's a library which allows the persistence of the information.
- [Alamofire](https://github.com/Alamofire/Alamofire) : It's a best http networking library used to handle the url requests and manage the http responses. 

#### Created by Ralph M.
