//
//  API.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

typealias PostsCallback = ([Post]?) -> ()
typealias UserCallback = (User?) -> ()
typealias CommentsCallback = ([Comment]?) -> ()

protocol APIProtocol {
    static func retrievePosts(force: Bool, completionHandler: @escaping (PostsCallback))
    static func retrieveUserData(userId: Int, completionHandler: @escaping (UserCallback))
    static func retrieveComments(postId:Int, completionHandler: @escaping (CommentsCallback))
}

struct API : APIProtocol {
    
    static private let apiURL = "https://jsonplaceholder.typicode.com/"
    static private func getHeaders() -> HTTPHeaders {
        return ["content-type": "application/json"]
    }
    
    static let postStore = PostStore()
    static let userStore = UserStore()
    static let commentStore = CommentStore()
    
    /// Fetch Posts
    static func retrievePosts(force: Bool, completionHandler: @escaping (PostsCallback)){
        if !force {
            postStore.loadStoredPosts { storedPosts in
                if let posts = storedPosts, posts.count > 0 {
                    let sorted = posts.sorted { $0.id < $1.id }
                    completionHandler(sorted)
                } else {
                    retrievePostsFromAPI { posts in
                        guard var networkPosts = posts else {
                            completionHandler(nil)
                            return
                        }
                        for i in 0...networkPosts.count {
                            if i == 20 {
                                break
                            }
                            networkPosts[i].isRead = false
                        }
                        storePosts(posts: networkPosts)
                        completionHandler(networkPosts)
                    }
                }
            }
        }else{
            retrievePostsFromAPI { posts in
                guard var networkPosts = posts else {
                    completionHandler(nil)
                    return
                }
                for i in 0...networkPosts.count {
                    if i == 20 {
                        break
                    }
                    networkPosts[i].isRead = false
                }
                storePosts(posts: networkPosts)
                completionHandler(networkPosts)
            }
        }
    }
    
    /// Get User From Post
    static func retrieveUserData(userId: Int, completionHandler: @escaping (UserCallback)){
        userStore.loadStoredUser(userId) { storedUser in
            if let user = storedUser{
                    completionHandler(user)
                } else {
                    retrieveUserDataFromAPI(userId: userId) { user in
                        guard let networkUser = user else {
                            completionHandler(nil)
                        return
                    }
                    storeUser(user: networkUser)
                    completionHandler(networkUser)
                }
            }
        }
    }
    
    /// Fetch Comments From Post
    static func retrieveComments(postId:Int, completionHandler: @escaping (CommentsCallback)){
        commentStore.loadStoredComments(postId) { storedComments in
            if let comments = storedComments, comments.count > 0 {
                completionHandler(comments)
            } else {
                retrieveCommentsFromAPI(postId: postId) { comments in
                    guard let networkComments = comments else {
                        completionHandler(nil)
                        return
                    }
                    storeComments(comments: networkComments)
                    completionHandler(networkComments)
                }
            }
        }
    }
    
}

private extension API {
    
    static func retrievePostsFromAPI(completionHandler: @escaping (PostsCallback)){
        let headers: HTTPHeaders = self.getHeaders()
        let url = apiURL + "posts/"
        AF.request(url, method: .get, headers:headers).response { response in
            DispatchQueue.main.async {
                switch response.result {
                    case .success:
                        guard let posts = try? JSONDecoder().decode([Post].self, from: response.data!) else { break }
                        completionHandler(posts)
                    break
                    case .failure(let error):
                        print(error.errorDescription!)
                        completionHandler(nil)
                    break
                }
            }
        }
    }
    
    static func retrieveUserDataFromAPI(userId: Int, completionHandler: @escaping (UserCallback)){
        let headers: HTTPHeaders = self.getHeaders()
        let url = apiURL + "users/" + String(userId)
        AF.request(url, method: .get, headers:headers).response { response in
            DispatchQueue.main.async {
                switch response.result {
                    case .success:
                        guard let user = try? JSONDecoder().decode(User.self, from: response.data!) else { break }
                        completionHandler(user)
                    break
                    case .failure(let error):
                        print(error.errorDescription!)
                        completionHandler(nil)
                    break
                }
            }
        }
    }
    
    static func retrieveCommentsFromAPI(postId:Int, completionHandler: @escaping (CommentsCallback)){
        let headers: HTTPHeaders = self.getHeaders()
        let url = apiURL + "comments?postId=" + String(postId)
        AF.request(url, method: .get, headers:headers).response { response in
            DispatchQueue.main.async {
                switch response.result {
                    case .success:
                        guard let comments = try? JSONDecoder().decode([Comment].self, from: response.data!) else { break }
                        completionHandler(comments)
                    break
                    case .failure(let error):
                        print(error.errorDescription!)
                        completionHandler(nil)
                    break
                }
            }
        }
    }
    
    static func storePosts(posts: [Post]) {
           postStore.realm = try! Realm()
           postStore.savePosts(posts)
    }
    
    static func storeUser(user: User) {
        userStore.realm = try! Realm()
        userStore.saveUser(user)
    }
    
    static func storeComments(comments: [Comment]) {
        commentStore.realm = try! Realm()
        commentStore.saveComments(comments)
    }
}
