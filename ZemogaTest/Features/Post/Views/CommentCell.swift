//
//  CommentCell.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    @IBOutlet weak var content : UILabel!
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var email : UILabel!

    var comment : Comment! {
        didSet{
            name.text = comment.name
            email.text = "(" + comment.email + ")"
            content.text = comment.body
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
