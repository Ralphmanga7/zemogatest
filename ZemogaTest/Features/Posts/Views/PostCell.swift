//
//  PostCell.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {
    
    @IBOutlet weak var dotStart : UIImageView!
    @IBOutlet weak var content : UILabel!
    
    var post : Post! {
        didSet {
            content.text = post.title
            dotStart.tintColor = .systemTeal
            dotStart.isHidden = false
            if post.isFavorite {
                dotStart.image = UIImage.init(named: "starFill")
                dotStart.tintColor = .yellow
            }else if !(post.isRead) {
                dotStart.image = UIImage.init(named: "dot")
            }else{
                dotStart.isHidden = true
            }
        }
    }
    
}
