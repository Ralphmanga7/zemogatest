
//
//  PostViewModel.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import Foundation

class PostViewModel {
    private(set) var userData: User?
    private(set) var comments: [Comment] = []
    
    var onDidLoadComments: (() -> Void)?
    var onDidLoadUserInfo: (() -> Void)?
    var onDidFailLoad : ((_ error:String) -> Void)?
    
    func loadUserData(userId: Int) {
        API.retrieveUserData(userId: userId) { [weak self] (user) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.userData = user
            strongSelf.onDidLoadUserInfo?()
        }
    }
    
    func loadComments(postId: Int) {
        API.retrieveComments(postId: postId, completionHandler: { [weak self] (comments) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.comments = comments ?? []
            strongSelf.onDidLoadComments?()
        })
    }
    
    func setFavorite(this post: Post, isFavorite: Bool) {
        API.postStore.setFavorite(post.managedObject(), isFavorite: isFavorite)
    }
    
}
