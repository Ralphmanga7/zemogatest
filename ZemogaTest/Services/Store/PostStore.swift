//
//  PostStore.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import Foundation
import RealmSwift

enum RuntimeError: Error {
    case NoRealmSet
}

typealias DeleteHandler = (Bool) -> Void

class PostStore {
    var realm: Realm?
    
    private func savePost(_ post: PostObject) throws {
        if realm != nil {
            try! realm!.write {
                realm!.add(post, update:.all)
            }
        } else {
            throw RuntimeError.NoRealmSet
        }
        
    }
    
    public func savePosts(_ posts: [Post]) {
        do {
            try posts.forEach({ post in
                do {
                    try savePost(post.managedObject())
                } catch RuntimeError.NoRealmSet {
                    print(RuntimeError.NoRealmSet.localizedDescription)
                }
            })
        } catch {
            print("Unexpected error")
        }
    }
    
    private func retrievePosts() throws -> Results<PostObject> {
        if realm != nil {
            return realm!.objects(PostObject.self)
        } else {
            throw RuntimeError.NoRealmSet
        }
    }
    
    private func retrievePost(_ id: Int) throws -> Results<PostObject> {
        if realm != nil {
            return realm!.objects(PostObject.self).filter("id = %i", id)
        } else {
            throw RuntimeError.NoRealmSet
        }
    }
    
    public func loadStoredPosts(completionHandler: @escaping PostsCallback) {
        realm = try! Realm()
        
        guard let realmPosts = try? retrievePosts() else {
            completionHandler(nil)
            return
        }
        
        let posts = Array(realmPosts).map { Post(managedObject: $0) }
        completionHandler(posts)
    }
    
    public func read(_ post: PostObject) {
        realm = try! Realm()
        
        let posts = try! retrievePost(post.id)
        try! realm!.write {
            posts.setValue(true, forKey: "isRead")
        }
    }
    
    public func setFavorite(_ post: PostObject, isFavorite: Bool) {
        realm = try! Realm()
        
        let posts = try! retrievePost(post.id)
        try! realm!.write {
            posts.setValue(isFavorite, forKey: "isFavorite")
        }
    }
    
    public func delete(_ post: PostObject, completionHandler: @escaping (DeleteHandler)) {
        realm = try! Realm()
        
        try! realm!.write {
            realm!.delete(realm!.objects(PostObject.self).filter("id=%i", post.id))
        }
        completionHandler(true)
    }
    
    public func deleteAll(completionHandler: @escaping (DeleteHandler)) {
        realm = try! Realm()
        
        try! realm!.write {
            realm!.delete(realm!.objects(PostObject.self))
        }
        completionHandler(true)
    }
}
