//
//  Comment.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import Foundation
import RealmSwift

struct Comment: Codable, Identifiable {
    var id: Int
    var postId: Int
    var name: String
    var email: String
    var body: String
}

// MARK: - Realm Comment
final class CommentObject: Object {
    @objc dynamic var id = -1
    @objc dynamic var postId = -1
    @objc dynamic var name = ""
    @objc dynamic var email = ""
    @objc dynamic var body = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

// MARK: - Comment Persistance
extension Comment: Persistable {
    public init(managedObject: CommentObject) {
        id = managedObject.id
        postId = managedObject.postId
        name = managedObject.name
        email = managedObject.email
        body = managedObject.body
    }
    
    public func managedObject() -> CommentObject {
        let commentObject = CommentObject()
        commentObject.id = id
        commentObject.postId = postId
        commentObject.name = name
        commentObject.email = email
        commentObject.body = body
        
        return commentObject
    }
}
