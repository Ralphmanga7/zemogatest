//
//  UserStore.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import Foundation
import RealmSwift

class UserStore {
    var realm: Realm?
    
    public func saveUser(_ user: User) {
       do {
            try saveUser(user.managedObject())
        } catch {
            print("Unexpected error")
        }
    }
    
    private func saveUser(_ user: UserObject) throws {
        if realm != nil {
            try! realm!.write {
                realm!.add(user, update:.all)
            }
        } else {
            throw RuntimeError.NoRealmSet
        }
    }
    
    private func retrieveUser(_ id: Int) throws -> Results<UserObject> {
        if realm != nil {
            return realm!.objects(UserObject.self).filter("id = %i", id)
        } else {
            throw RuntimeError.NoRealmSet
        }
    }
    
    public func loadStoredUser(_ id: Int, completion: @escaping UserCallback) {
        realm = try! Realm()
        
        guard let realmUsers = try? retrieveUser(id) else {
            completion(nil)
            return
        }
        
        let user = Array(realmUsers).map { User(managedObject: $0) }.first
        
        completion(user)
    }
    
    public func deleteAll(completion: @escaping (DeleteHandler)) {
        realm = try! Realm()
        
        try! realm!.write {
            realm!.delete(realm!.objects(UserObject.self))
        }
        completion(true)
    }
}
