//
//  Post.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import Foundation
import RealmSwift

struct Post : Codable, Identifiable, Equatable{
    
    let id : Int
    let userId : Int
    let title : String
    let body : String
    var isFavorite : Bool = false
    var isRead : Bool = true
    
    enum CodingKeys: String, CodingKey {
        case userId
        case id
        case title
        case body
    }
}

// MARK: - Realm Post
final class PostObject: Object {
    @objc dynamic var id = -1
    @objc dynamic var userId = -1
    @objc dynamic var title = ""
    @objc dynamic var body = ""
    @objc dynamic var isFavorite = false
    @objc dynamic var isRead = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

// MARK: - Post Persistance
extension Post: Persistable {
    public init(managedObject: PostObject) {
        id = managedObject.id
        userId = managedObject.userId
        title = managedObject.title
        body = managedObject.body
        isFavorite = managedObject.isFavorite
        isRead = managedObject.isRead
    }
    
    public func managedObject() -> PostObject {
        let postObject = PostObject()
        postObject.id = id
        postObject.userId = userId
        postObject.title = title
        postObject.body = body
        postObject.isFavorite = isFavorite
        postObject.isRead = isRead
        
        return postObject
    }
}
