//
//  CommentStore.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import Foundation
import RealmSwift

class CommentStore {
    var realm: Realm?
    
    public func saveComments(_ comments: [Comment]) {
        do {
            try comments.forEach({ comment in
                do {
                    try saveComment(comment.managedObject())
                } catch RuntimeError.NoRealmSet {
                    print(RuntimeError.NoRealmSet.localizedDescription)
                }
            })
        } catch {
            print("Unexpected error")
        }
    }
    
    private func saveComment(_ comment: CommentObject) throws {
        if realm != nil {
            try! realm!.write {
                self.realm!.add(comment, update:.all)
            }
        } else {
            throw RuntimeError.NoRealmSet
        }
        
    }
    
    private func retrieveComments(_ postId: Int) throws -> Results<CommentObject> {
        if realm != nil {
            return realm!.objects(CommentObject.self).filter("postId = %i", postId)
        } else {
            throw RuntimeError.NoRealmSet
        }
    }
    
    private func retrieveComments() throws -> Results<CommentObject> {
        if realm != nil {
            return realm!.objects(CommentObject.self)
        } else {
            throw RuntimeError.NoRealmSet
        }
    }
    
    public func loadStoredComments(_ postId: Int, completion: @escaping CommentsCallback) {
        realm = try! Realm()
        
        guard let realmComments = try? retrieveComments(postId) else {
            completion(nil)
            return
        }
        
        let comments = Array(realmComments).map { Comment(managedObject: $0) }
        
        completion(comments)
    }
    
    public func deleteAll(completion: @escaping (DeleteHandler)) {
        realm = try! Realm()
        
        try! realm!.write {
            realm!.delete(realm!.objects(CommentObject.self))
        }
        completion(true)
    }
    
}
