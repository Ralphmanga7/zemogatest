//
//  PostsViewModel.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import Foundation


class PostsViewModel {
    private(set) var posts: [Post] = []
    private(set) var favorites: [Post] = []
    var onDidLoadPosts: (() -> Void)?
    var onDidFailLoad : ((_ error:String) -> Void)?
    
    func getPosts(force : Bool = false){
        API.retrievePosts(force: force) { [weak self] (posts) in
            guard let strongSelf = self else { return }
            strongSelf.posts = posts ?? []
            if strongSelf.posts.count == 0 {
                strongSelf.onDidFailLoad?("No Posts")
            } else {
                strongSelf.getFavorites()
                strongSelf.onDidLoadPosts?()
            }
        }
    }
    
    func getFavorites() {
        favorites = posts.filter { $0.isFavorite }
        onDidLoadPosts?()
    }
    
    func setFavorite(post:Post, favorite:Bool) {
        API.postStore.setFavorite(post.managedObject(), isFavorite: favorite)
    }
    
    func markAsRead(post: Post) {
        API.postStore.read(post.managedObject())
    }
    
    func delete(post: Post) {
        if let index = posts.firstIndex(of: post) {
            API.postStore.delete(post.managedObject()){ deleted in
                self.posts.remove(at: index)
                if let indexFav = self.favorites.firstIndex(of: post) {
                    self.favorites.remove(at: indexFav)
                }
            }
        }
    }
    
    func deleteAll() {
        API.postStore.deleteAll { deleted in
            self.posts.removeAll()
            self.favorites.removeAll()
            self.onDidLoadPosts?()
        }
    }
}
