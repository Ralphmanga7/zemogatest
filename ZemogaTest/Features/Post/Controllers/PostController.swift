//
//  PostController.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.s
//  Copyright © 2020 R4lph. All rights reserved.
//

import UIKit

class PostController: UIViewController {
    
    @IBOutlet weak var postTitle : UILabel!
    @IBOutlet weak var content : UILabel!
    @IBOutlet weak var user : UILabel!
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var email : UILabel!
    @IBOutlet weak var phone : UILabel!
    @IBOutlet weak var website : UILabel!
    @IBOutlet weak var commentsTable : UITableView!
    private let postViewModel = PostViewModel()
    var dataSource : [Comment]! = []
    var post : Post!
    var retrivedAllData = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        configBindings()
        CommonUtil.addLoading(from: self.view)
        postViewModel.loadUserData(userId: post.userId)
        postViewModel.loadComments(postId: post.id)
    }
    
    func setupView(){
        let nib = UINib.init(nibName: "CommentCell", bundle: nil)
        commentsTable.register(nib, forCellReuseIdentifier: "CommentCell")
        commentsTable.sectionHeaderHeight = UITableView.automaticDimension
        commentsTable.estimatedSectionHeaderHeight = 60
        self.postTitle.text = post.title
        self.content.text = post.body
        updateButton()
    }
    
    func configBindings() {
        postViewModel.onDidLoadUserInfo = {
            self.retrivedAllData += 1
            self.updateUserData()
            self.removeLoading()
        }
        postViewModel.onDidLoadComments = {
            self.retrivedAllData += 1
            self.dataSource = self.postViewModel.comments
            self.commentsTable.reloadWithAnimation()
            self.removeLoading()
        }
    }
    
    func removeLoading(){
        if retrivedAllData == 2 {
            CommonUtil.removeLoading()
        }
    }
    
    func updateUserData(){
        name.text = "Name: " + postViewModel.userData!.name
        email.text = "Email: " + postViewModel.userData!.email
        phone.text = "Phone: " + postViewModel.userData!.phone
        website.text = "Website: " + postViewModel.userData!.website
    }
    
    @IBAction func markFavorite(sender: UIBarButtonItem){
        navigationItem.rightBarButtonItem = nil
        postViewModel.setFavorite(this: post, isFavorite: !post.isFavorite)
        post.isFavorite = !post.isFavorite
        updateButton()
    }
    
    func updateButton(){
        let imageName = post.isFavorite ? "starFill" :"star"
        let img = UIImage(named: imageName)
        let button = UIBarButtonItem(image: img, style: .plain, target: self, action: #selector(markFavorite(sender:)))
        navigationItem.setRightBarButton(button, animated: true)
    }
    
}

extension PostController :  UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "COMMENTS"
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 121
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
        cell.comment = dataSource[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
}
