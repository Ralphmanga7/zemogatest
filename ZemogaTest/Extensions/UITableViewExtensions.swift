//
//  UITableViewExtensions.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import UIKit

extension UITableView {
    func reloadWithAnimation() {
        self.reloadData()
        let cells = self.visibleCells
        var delayCounter = 0
        for cell in cells {
            cell.alpha = 0
        }
        for cell in cells {
            UIView.animate(withDuration: 0.4, delay: 0.02 * Double(delayCounter), options: .curveEaseInOut, animations: {
                cell.alpha = 1
            }, completion: nil)
            delayCounter += 1
        }
    }
}
