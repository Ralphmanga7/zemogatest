//
//  CommonUtil.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import UIKit
class CommonUtil: NSObject {
    
    static let loading = Loading.instanceFromNib() as? Loading
    
    class func showAlert(_ alertTitle: String, alertContent: String, fromViewController: UIViewController, actionTitle: String) {
        let alert = UIAlertController(title: alertTitle, message: alertContent, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertAction.Style.default, handler: nil))
        fromViewController.present(alert, animated: true, completion: nil)
    }
    
    class func addLoading(from:UIView){
        loading?.frame = CGRect(x: from.bounds.midX - loading!.bounds.midX,y: from.bounds.midY - loading!.bounds.midY, width: loading!.bounds.width,height: loading!.bounds.height)
        from.addSubview(loading!)
    }
    
    class func removeLoading(){
        loading?.removeFromSuperview()
    }
}
