//
//  User.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import Foundation
import RealmSwift

struct User : Codable, Identifiable {
    var id: Int
    var name: String
    var email: String
    var phone: String
    var website: String
}

// MARK: - Realm User
final class UserObject: Object {
    @objc dynamic var id = -1
    @objc dynamic var name = ""
    @objc dynamic var email = ""
    @objc dynamic var phone = ""
    @objc dynamic var website = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

// MARK: - User Persistance
extension User: Persistable {
    public init(managedObject: UserObject) {
        id = managedObject.id
        name = managedObject.name
        email = managedObject.email
        phone = managedObject.phone
        website = managedObject.website
    }
    
    public func managedObject() -> UserObject {
        let userObject = UserObject()
        userObject.id = id
        userObject.name = name
        userObject.email = email
        userObject.phone = phone
        userObject.website = website
        
        return userObject
    }
}
