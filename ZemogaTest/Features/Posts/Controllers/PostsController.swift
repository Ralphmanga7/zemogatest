//
//  PostsController.swift
//  ZemogaTest
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import UIKit

class PostsController: UIViewController {
    
    @IBOutlet weak var segmentView: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    private let postsViewModel = PostsViewModel()
    var selectedPost : Post!
    var dataSource : [Post]! = []

    override func viewDidLoad() {
        super.viewDidLoad()
        configBindings()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.addLoading(from: self.view)
        postsViewModel.getPosts()
    }
    
    func setupView(){
        let nib = UINib.init(nibName: "PostCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "PostCell")
    }
    
    func configBindings() {
        postsViewModel.onDidLoadPosts = {
            self.updateDataSource()
            self.tableView.reloadWithAnimation()
            CommonUtil.removeLoading()
        }
        postsViewModel.onDidFailLoad = { error in
            CommonUtil.removeLoading()
            CommonUtil.showAlert("Error", alertContent: error, fromViewController: self, actionTitle: "OK")
        }
    }
    
    func updateDataSource(){
        dataSource = segmentView.selectedSegmentIndex == 0 ? postsViewModel.posts : postsViewModel.favorites
    }
    
    @IBAction func reloadPosts(){
        CommonUtil.addLoading(from: self.view)
        postsViewModel.getPosts(force: true)
    }
    
    @IBAction func segmentChanged(){
        updateDataSource()
        tableView.reloadWithAnimation()
    }
    
    @IBAction func deleteAllAction(_ sender: Any) {
        let alert = UIAlertController(title: "Are you sure?", message: "Are you sure you want to delete all posts?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes, Delete", style: .destructive, handler:{ (alert) in
            self.postsViewModel.deleteAll()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "post" {
            let postController = segue.destination as! PostController
            postController.post = selectedPost
        }
    }
}


extension PostsController :  UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPost = dataSource[indexPath.row]
        postsViewModel.markAsRead(post: selectedPost)
        self.performSegue(withIdentifier: "post", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension PostsController :  UITableViewDataSource{
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 69
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        cell.post = dataSource[indexPath.row]
        return cell
    }
    
}

//Deletion
extension PostsController {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        return
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let contextItem = UIContextualAction(style: .destructive, title: "Delete") {  (contextualAction, view, boolValue) in
            self.postsViewModel.delete(post: self.dataSource[indexPath.row])
            self.updateDataSource()
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        let swipeActions = UISwipeActionsConfiguration(actions: [contextItem])

        return swipeActions
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}
