//
//  ZemogaTestTests.swift
//  ZemogaTestTests
//
//  Created by Ralph on 20/04/20.
//  Copyright © 2020 R4lph. All rights reserved.
//

import XCTest
@testable import ZemogaTest

class ZemogaTestTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func loadPostsTest() {
        
        let retrivePostsTest = expectation(description: "retrivePostsTest")
        
        API.retrievePosts(force: true) { posts in
            XCTAssertEqual(posts!.first?.id, 1) // IS SORTED
            XCTAssertTrue(posts!.count > 20) // HAS ALL POSTS
            XCTAssertEqual(posts!.first?.isFavorite, false) // NO FAVORTIES
            XCTAssertTrue(posts![21].isRead) // > 20 READ
            XCTAssertTrue(posts![30].isRead) // > 20 READ
            XCTAssertFalse(posts![20].isRead) // < 20 NOT READ
            XCTAssertFalse(posts![8].isRead) // < 20 NOT READ
            retrivePostsTest.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
    }

}
